let express = require('express');
let path = require('path');
let favicon = require('serve-favicon');
let logger = require('morgan');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
let nunjucks = require('nunjucks');
let session = require('express-session');
let RedisStore = require('connect-redis')(session);
let main = require('./routes/main');
let API = require('./routes/api'); 
let socket= require('./routes/socket'); 
let app = express();
let env = nunjucks.configure('views', {
    autoescape: false,
    express: app,
    noCache:false,
    watch:true
}); 

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
    secret: 'fx',
    name: 'fx.sid',
    rolling: true,
    cookie: {maxAge: 2 * 60 * 60 * 1000},//12小时
    //cookie: {maxAge: 3000},
    resave: false,
    token:{},
    saveUninitialized: true,
//    store: null
    store: new RedisStore({
        "host": (process.env.REDIS_IP || "127.0.0.1"),
        "port": (process.env.REDIS_PORT || "6378"),
        "pass": (process.env.REDIS_PASS || ""),
        "db": 3,
        "ttl": 2 * 60 * 60,
        "logErrors": true
    })
}));

app.use('/', main);
app.use('/api', API);
app.use('/api', socket);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
