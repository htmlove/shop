let express = require('express');
let router = express.Router();
let fs=require('fs');
let url = require('url');
let base_url=process.env.SERVER_URL;
let title="分销系统";
router.get('*', function(req, res, next) {
	var URL=url.parse(req.url);
	var path=URL.pathname;
	pageRoute();
    function pageRoute(){
    	switch(path){
    		case '/':
    		  res.render('main', { 
			  	title: title,
			  	base_url:base_url
			  });
			 break; 
			case '/main':		 
			  res.render('main', { 
			  	title: title,
			  	base_url:base_url
			  });
			break;
			default:
			  res.render(path.substring(1),{
			  	title:title,
			  	base_url:base_url
			  })	
			 break;
		}
    }

  
});
module.exports = router;
