var myChart;

var ChartType = 1;//图表渲染类型


//渲染百度图表视图方法
function Chartview(url,datajson){
	BL.ajax({
	    url:url,
	    type:'get',
	    data:datajson,
	    success:function(Data){
		    console.log(Data);
		    var seriesArr = [];
		    var legendArr = Data.y_axis;
		    var xAxisArr = Data.x_axis;
		    for(var i=0;i<Data.y_axis.length;i++){
		    	var series_json ={};
		    	series_json.name = Data.y_axis[i];
		    	series_json.type = 'bar';
		    	series_json.data = Data.values[i];
		    	seriesArr.push(series_json);
		    }

		  	var option = {
		   	    tooltip : {
		   	        trigger: 'axis',
		   	        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
		   	            type : 'line'        // 默认为直线，可选为：'line' | 'shadow'
		   	        }
		   	    },
		   	    toolbox:{
		   	        show : true,
		   	        feature : {
		   	            mark : {
		   	            	show: true,
		   	            	title : {
		   		                mark : '辅助线开关',
		   		                markUndo : '删除辅助线',
		   		                markClear : '清空辅助线'
		   		            }
		   	            },
		   	            dataView : {show: true, readOnly: false},
		   	            magicType : {
		   	            	show: true, 
		   	            	type: ['line','bar'],
		   	            	title : {
		   	                    line : '折线图切换',
		   	                    bar : '柱形图切换'
		   	                }
		   	            },
		   	            restore : {show: true},
		   	            saveAsImage : {show: true}
		   	        },
		   	        x:'right',
		   	        y:'bottom'
		   	    },
		   	    legend: {
		   	    	x:'left',
		   	    	y:'top',
		   	        data:legendArr
		   	    },
		   	    xAxis : [
		   	        {
		   	            type : 'category',
		   	            data : xAxisArr
		   	        }
		   	    ],
		   	    yAxis : [
		   	        {
		   	            type : 'value'
		   	        }
		   	    ],
	   	    	series : seriesArr
	   		};
	   		//渲染图表
	   		console.log(option)
	   		myChart.setOption(option);
		},
		error:function(res,msg){
			console.log(msg)
		}
    })
}	

