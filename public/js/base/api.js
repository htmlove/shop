var API={
	user:{
		list: window.CONFIG.base_url + '/user/pageList',//用户列表
		info: window.CONFIG.base_url + '/user/getUser/',//用户详情
		delete: window.CONFIG.base_url + '/user/remove/',//删除用户
		edit: window.CONFIG.base_url + '/user/modifyUser',//编辑用户
		add: window.CONFIG.base_url + '/user/addUser',//添加用户
		password: window.CONFIG.base_url + '/user/modifyPaw',//修改密码
		integral:window.CONFIG.base_url + '/user/integral',//用户积分
	},
	address:{
		add:window.CONFIG.base_url + '/userAddress/newAddress',//添加地址
		edit:window.CONFIG.base_url + '/userAddress/modifyAddress',//修改地址
		info:window.CONFIG.base_url + '/userAddress/getUserAddress/',//地址详情
		delete:window.CONFIG.base_url + '/userAddress/remove/',//删除地址
	}	
}	