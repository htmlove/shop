var renderForm=function(arr){
	    var _html='<div class="layui-form-item" style="margin-top:15px"><form class="layui-form" action=""  id="layuiForm" lay-filter="layuiForm">';
		var render = {
	        hidden:function (opt) {
	            var _html = '<input type="hidden"' +
	                        function(){
	                            var str="";
	                            str+= (opt.id?'id="' + opt.id + '"' : '');
	                            str+=(opt.name?'name=' + '"' + opt.name + '"':'');
	                            str+= (opt.value?' value=' + '"' + opt.value +'"':'');
	                            return str;
	                        }();
	                        _html+='>';
	                        return _html;
	        },
	        input: function (opt) {
	        	var _html = '<div class="layui-input-inline">' +
	                        ' <input ' +  
	                        function(){
	                            var str="";
	                            str+='autocomplete="off"';
	                            str+=(opt.name?'name=' + '"' + opt.name + '"':'');
	                            str+=(opt.disabled?'disabled=' + '"' + opt.disabled + '"':'');                         
	                            str+=(opt.readonly?'readonly=' + '"' + opt.readonly + '"':'');
	                            str+=(opt.value?'value=' + '"' + opt.value + '"':'');
	                            str+='type="' + (opt.type||"text") + '"';
	                            str+=(opt.id?'id=' + '"' + opt.id + '"':''); 
	                            str+='class="layui-input ' + (opt.class||"") + '"';
	                            str+= (opt.verify==true?' lay-verify="required"':'');
	                            str+= (typeof opt.verify=="string"?' lay-verify=' + '"' + opt.verify + '"':"");
	                            str+=(opt.placeholder?'placeholder=' + '"' + opt.placeholder + '"':'');
	                            str+=(opt.attr||"");
	                            return str;
	                        }();                     
	                        _html+='>';
	                        _html +=  '</div>';
	                        return _html;

	        },
	        password: function (opt) {
	        	var _html = '<div class="layui-input-inline">' +
	                        ' <input ' +  
	                        function(){
	                            var str="";
	                            str+='autocomplete="off"';
	                            str+=(opt.name?'name=' + '"' + opt.name + '"':'');
	                            str+=(opt.disabled?'disabled=' + '"' + opt.disabled + '"':'');                         
	                            str+=(opt.readonly?'readonly=' + '"' + opt.readonly + '"':'');
	                            str+=(opt.value?'value=' + '"' + opt.value + '"':'');
	                            str+='type="password"';
	                            str+=(opt.id?'id=' + '"' + opt.id + '"':''); 
	                            str+='class="layui-input ' + (opt.class||"") + '"';
	                            str+= (opt.verify==true?' lay-verify="required"':'');
	                            str+= (typeof opt.verify=="string"?' lay-verify=' + '"' + opt.verify + '"':"");
	                            str+=(opt.placeholder?'placeholder=' + '"' + opt.placeholder + '"':'');
	                            str+=(opt.attr||"");
	                            return str;
	                        }();                     
	                        _html+='>';
	                        _html +=  '</div>';
	                        return _html;

	        },
	        "select": function (opt) {
	            var select=opt.select||null;
	            var checkbox=opt.checkbox||null;
	            var _html = ' <div class="layui-input-inline layui-input-inlineReset">' + '<select ' + 
	                    function(){
	                        var str="";
	                        str+=(opt.class?' class=' + '"' + opt.class + '"':'');
	                        str+=(opt.id?' id=' + '"' + opt.id + '"':'');
	                        str+=(opt.multiple?'multiple="multiple"':'');
	                        str+=(opt.name?'name=' + '"' + opt.name +'"':'');
	                        str+=(opt.verify?'lay-verify="required"':'');
	                        str+='>';
	                        str+=(opt.defalut==false?"":'<option value="">请选择</option>');
	                        return str;
	                    }();
	                    for(var i=0;i<opt.options.length;i++){
	                    	_html+='<option value="' + opt.options[i].value + '">' + opt.options[i].text +'</option>';
	                    }		            
	            _html += '</select>' +'</div>';
	            return _html;
	        },
	        date:function (opt) {
	            var _html = '<div class="layui-input-inline">' +
	                        '<input type="text"' + 
	                        function(){
	                            var str="";
	                            str+='autocomplete="off"';
	                            str+= (opt.value?'value=' + '"' + opt.value +'"':'');
	                            str+= (opt.placeholder?'placeholder=' + '"' + opt.placeholder +'"':'');
	                            str+= (opt.name?'name=' + '"' + opt.name +'"':'');
	                            str+='class="layui-input bl_date' + (opt.class||"") + '"';
	                            str+= (opt.verify==true?' lay-verify="required"':'');
	                            str+= (opt.id?'id="' + opt.id + '"' : '');
	                            str+=' data-type="date"';
	                            str+=' data-min="' + (opt.min||'') + '"';
	                            return str
	                        }();
	                        _html+='>';
	                        _html += '<i class="layui-icon layui-input-icon">&#xe637;</i>';  
	                        _html +=  '</div>';
	                        return _html;
	        },
	        title:function(title,verify){
	  		    var _html='<label class="layui-form-label layui-form-labelReset">' + (verify?'<span class="layui-red">*</span>':'') + title + '</label>';
	  			return _html;
	  		}
	}        

	for(var i =0 ;i<arr.length ;i++){
  		var item =arr[i].content;
  		for(var j=0;j<item.length;j++){
  			if(item[j].type=='hidden'){
  				_html+=render[item[j].type](item[j]);
  				continue;
  			}
  			_html+='<div class="layui-inline">';
  		    _html+=render.title(arr[i].title.name,item[j].verify||false);
  		    _html+=render[item[j].type](item[j]);
  		    _html+='</div>';
  		}
  		
  	}
  	_html+='<button lay-filter="lay-submit" id="lay_submit_btn" lay-submit="" style="display:none"></button></form></div>';
  	return _html;

};
