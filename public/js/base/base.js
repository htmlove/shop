;(function(win){
	win.FX=FX={
		getWinHeight:function(n){
			var height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight; 
		    var n=n||0
	        return height-n;
		},
		/*获取浏览器宽度减去偏移量*/
		getWinWidth:function(n){
			var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth; 
	        var n=n||0
            return width-n;
		},
		//阻止冒泡
		stopPropagation:function(e){
			if (e.stopPropagation) 
			e.stopPropagation(); 
			else 
			e.cancelBubble = true; 
		},
		verify:function(val,rules){
			var r=rules.split('|');
			var verify={
				number:function(v){//判断是否两位小数
					return (!isNaN(v)&&v>=0)?true:'请输入大于0的数字';			
				},
				phone:function(v){
					if(!v) return true;
		            return /^[1][3,4,5,7,6,8,9][0-9]{9}$/.test(v)?true:'请输入正确的手机号';
		        },
		        email:function(v){
		           return /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(v)?true:'邮箱格式不正确';
		        },
		        isTwoDecimal:function(v){
		        	return /^\d+(\.\d{1,2})?$/.test(v)?true:'请输入数字或者两位小数';
		        },
		        isCardID(v){
				   var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;  
				   return reg.test(v) === false? "请输入正确的身份证号码" : true;
				}  		
			}
			for(var i=0;i<r.length;i++){
				var temp=verify[r[i]](val);
				if(typeof temp!='boolean'){
					return temp;
					break;
				}
			}
			return true;
		},
		//密码强度校验
		password:function checkPass(pass){ 
			return /^(?![a-zA-Z]+$)(?![a-z\d]+$)(?![a-z!@#\$%]+$)(?![A-Z\d]+$)(?![A-Z!@#\$%]+$)(?![\d!@#\$%]+$)[a-zA-Z\d!@#\$%]{8,20}/.test(pass);
		}, 
		//日期转时间戳 (毫秒)
		getTimeToStamp:function(d){
		    d = d.replace(new RegExp("-","gm"),"/");
		    var ss = (new Date(d)).getTime();
		    return ss;
		},
		//时间戳转日期
		timestampToTime:function(timestamp) {
	        var date = new Date(timestamp);
			var Y = date.getFullYear() + '-';
			var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
			var D = (date.getDate()<10?'0'+date.getDate():date.getDate());
			return Y+M+D; 
	    },
		//日期排序
		dateStrDesc:function(arr){
			var a=[];
			var b=[];
			for(var i=0;i<arr.length;i++){
				a.push(BL.getTimeToStamp(arr[i]));
				a.sort();
			}
			for(var i=0;i<a.length;i++){
				b.push(BL.timestampToTime(a[i]));
			}
			return b;
			
		},
		//时分秒排序
		timeStrDesc:function(arr){
			var a=[];
			var c=[];
			for(var i=0;i<arr.length;i++){
				a.push(arr[i].split(':')[0]+arr[i].split(':')[1]+arr[i].split(':')[2]);
			}
			var b=a.sort();
			for(var i=0;i<b.length;i++){
				var result="";
				for(var j=0;j<b[i].length;j++){
					result+=b[i][j];
					if(j % 2 == 1&&j!=b[i].length-1) result += ':';
				}
				c.push(result);
			}
			return c;
		},
		//数组去重复
		unique:function(arr){
		 var res = [];
		 var json = {};
		 for(var i = 0; i < arr.length; i++){
		  if(!json[arr[i]]){
		   res.push(arr[i]);
		   json[arr[i]] = 1;
		  }
		 }
		 return res;
		},
		//判断数据是对象还是数组 
		isObject:function(e){
			if(o){
			 return o instanceof Array && o instanceof Object;
			}else{
				return null;
			}
		},
		//是否是数组
		isArray: function(arr) {
			return Object.prototype.toString.apply(arr) === "[object Array]";
		},
		//截取时间字符串
		getTimeStr:function(str,type){
			switch(type){
				case 'mm-dd':
				return str?str.substring(0,10):'';
				break;
				case 'time':
				return str?str.substring(11,19):'';
				break;
				case 'date_time':
				return str?str.substring(0,16):'';
				break;
				case 'yyyy-mm-dd':
				return str?str.substring(0,10):'';
				break;
				case 'yyyy-mm-dd hh:mm':
				return str?str.substring(0,16):'';
				break;
				case 'hh:mm':
				return str?str.substring(11,16):'';
				break;
			}
		},
		//排序
		getSort:function(key,desc) {//desc true 降序  false 升序
			return function(a,b){
			　　return desc ? ((parseInt(a[key]) < parseInt(b[key]))?1:((parseInt(a[key]) > parseInt(b[key]))?-1:0)):((parseInt(a[key]) < parseInt(b[key]))?-1:((parseInt(a[key]) > parseInt(b[key]))?1:0));
			}
		},
		//获取URL参数
		getQueryString:function(name){
			//var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
		    //var r = window.location.search.substr(1).match(reg);
		    //if(r!=null)return  unescape(r[2]); return null;
		    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(window.location.href) || [, ''])[1].replace(/\+/g, '%20')) || null
		},
		//去除小数部分后面多余的0
		parseFloat:function(str){
			if(str){
				return parseFloat(str);
			}else{
				return "";
			}
		},
		//根据出生日期计算年龄
		ages:function(str){   
			if(!str) {return false};
	        var r=str.match(/^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/);     
	        if(r==null)return   false;     
	        var   d=new Date(r[1],r[3]-1,r[4]);     
	        if (d.getFullYear()==r[1]&&(d.getMonth()+1)==r[3]&&d.getDate()==r[4]){   
	          var   Y   =   new   Date().getFullYear();   
	          return (Y-r[1]);   
	        }   
	        return null;   
  		}, 
		//隐藏电话号码星号代替
		telHide:function(tel){
			if(tel){
				return tel.replace(/(\d{3})(\d{4})(\d{4})/,"$1****$3")
			}else{
				return '';
			}
		},
		//保留两位小数
		toFixed:function(v){
			v=v*1;
			return isNaN(v)?"":v.toFixed(2);
		},
		//字符串去重复
		strTorepeat:function(str){
			var newStr=str.split(',');
			var arr=[];
			for(var i=0;i<newStr.length;i++){
				if(arr.indexOf(newStr[i])==-1){
					arr.push(newStr[i]);
				}
			}
			return arr.join(',');
		},
		//获取obj length
		getObjLength:function(obj) {
	 　　var count = 0;
		    for(var i in obj){
		        if(obj.hasOwnProperty(i)){//如果包含除它的原型本身之外的属性
		            count++;
		        };
		    };
		    return count;   

		 },
		//暂无数据 
		getEmptyDom:function(str,icon){
			return '<div class="empty">'+ (icon?icon:'<img src="../../images/no.png">') +'<p class="empty_text">'+(str||'暂无数据~')+'<p></div>';
		},
		//获取某个日期是星期几
		getWeekNum:function(str){  
			    var dt = new Date(str.replace(/-/g, '/'));
			    var a = ['星期日', '星期一','星期二','星期三','星期四','星期五','星期六'];
			    return a[dt.getDay()];
        },
        //获取本周,下周,上周,开始时间,和结束时间
        getWeekTimes:function(type){   	
        	switch (type){
        		case 'prev':
					var start=moment().subtract(1, 'weeks').day(1).format('YYYY-MM-DD');
					var end=moment().subtract(1, 'weeks').day(7).format('YYYY-MM-DD');
					return {firstDay:start,lastDay:end};
        		break;
        		case 'this':
        		    var weekOfday = moment().format('E');//计算今天是这周第几天
	        		var start = moment().subtract(weekOfday-1, 'days').format('YYYY-MM-DD');
					var end = moment().subtract(weekOfday-7, 'days').format('YYYY-MM-DD');
					return {firstDay:start,lastDay:end};
        		break;
        		case 'next':
        		    var start=moment().subtract(-1, 'weeks').day(1).format('YYYY-MM-DD');
					var end=moment().subtract(-1, 'weeks').day(7).format('YYYY-MM-DD');
					return {firstDay:start,lastDay:end};
        		break

        	}
        	
        },
        //获取当前时间，格式YYYY-MM-DD
	    getNowFormatDate:function() {
	        var date = new Date();
	        var seperator1 = "-";
	        var year = date.getFullYear();
	        var month = date.getMonth() + 1;
	        var strDate = date.getDate();
	        if (month >= 1 && month <= 9) {
	            month = "0" + month;
	        }
	        if (strDate >= 0 && strDate <= 9) {
	            strDate = "0" + strDate;
	        }
	        var currentdate = year + seperator1 + month + seperator1 + strDate;
	        return currentdate;
	    },
        //获取两个时间段之间的所有日期
		getAllDate:function(begin, end){
		  Date.prototype.format = function() {  
		      var s = '';  
		      var mouth = (this.getMonth() + 1)>=10?(this.getMonth() + 1):('0'+(this.getMonth() + 1));  
		      var day = this.getDate()>=10?this.getDate():('0'+this.getDate());  
		      s += this.getFullYear() + '-'; // 获取年份。  
		      s += mouth + "-"; // 获取月份。  
		      s += day; // 获取日。  
		      return (s); // 返回日期。  
		  }; 
		  var ab = begin.split("-");  
	      var ae = end.split("-");  
	      var db = new Date();  
	      db.setUTCFullYear(ab[0], ab[1] - 1, ab[2]);  
	      var de = new Date();  
	      de.setUTCFullYear(ae[0], ae[1] - 1, ae[2]);  
	      var unixDb = db.getTime();  
	      var unixDe = de.getTime(); 
	      var arr=[]; 
	      for (var k = unixDb; k <= unixDe;) {  
	          arr.push((new Date(parseInt(k))).format());  
	          k = k + 24 * 60 * 60 * 1000;  
	      }  
	      return arr;
		},
		
		//判断两个对象得值是否相等
		isObjectValueEqual:function(a, b) {
		    var aProps = Object.getOwnPropertyNames(a);
		    var bProps = Object.getOwnPropertyNames(b);
		    if (aProps.length != bProps.length) {

		        return false;

		    }
		    for (var i = 0; i < aProps.length; i++) {
		        var propName = aProps[i];
		        if (a[propName] !== b[propName]) {
		            return false;
		        }
		    }
		    return true;
		},
		//判断字符串是否是日期格式
		isDate:function(dateString){
		  if(dateString.trim()=="")return false;
		  var r=dateString.match(/^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/); 
		  if(r==null){
		   //alert("请输入格式正确的日期\n\r日期格式：yyyy-mm-dd\n\r例  如：2008-08-08\n\r");
		  return false;
		  }
		  var d=new Date(r[1],r[3]-1,r[4]);  
		  var num = (d.getFullYear()==r[1]&&(d.getMonth()+1)==r[3]&&d.getDate()==r[4]);
		  if(num==0){
		   //alert("请输入格式正确的日期\n\r日期格式：yyyy-mm-dd\n\r例  如：2008-08-08\n\r");
		  }
		  return (num!=0);
		}, 
		//分解URL
		parseURL:function(url){
			 var url=unescape(url);
			 var a =  document.createElement('a');  
			 a.href = url;  
			 return {  
			 source: url,  
			 protocol: a.protocol.replace(':',''),  
			 host: a.hostname,  
			 port: a.port,  
			 query: a.search,  
			 params:(function(){
			 	var ret = {},   
				u=url.split('?')[1]?url.split('?')[1].split('#')[0].split('&'):[];
				for(var i=0;i<u.length;i++){
					ret[u[i].split('=')[0]]=BL.getQueryString(u[i].split('=')[0])
				}
				return ret;
			 })(),
			 file: (a.pathname.match(/\/([^\/?#]+)$/i) || [,''])[1],  
			 hash: a.hash.replace('#',''),  
			 path: a.pathname.replace(/^([^\/])/,'/$1'),  
			 relative: (a.href.match(/tps?:\/\/[^\/]+(.+)/) || [,''])[1],  
			 segments: a.pathname.replace(/^\//,'').split('/')  
			 };  
		},
		//全屏显示
		toggleFullScreen:function() {
			if (!document.fullscreenElement && // alternative standard method
				!document.mozFullScreenElement && !document.webkitFullscreenElement) {// current working methods
				if (document.documentElement.requestFullscreen) {
					document.documentElement.requestFullscreen();
				} else if (document.documentElement.mozRequestFullScreen) {
					document.documentElement.mozRequestFullScreen();
				} else if (document.documentElement.webkitRequestFullscreen) {
					document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
				}
			} else {
				if (document.cancelFullScreen) {
					document.cancelFullScreen();
				} else if (document.mozCancelFullScreen) {
					document.mozCancelFullScreen();
				} else if (document.webkitCancelFullScreen) {
					document.webkitCancelFullScreen();
				}
			}
		},
		//全屏切换改变报表表格定位
		changeIframeTable:function(){
			var iframs=document.getElementsByTagName("iframe");
		  	 for(var i=0;i<iframs.length;i++){
		  		 if(iframs[i].contentWindow.changeHtml){
		  			!function(ele){
		  				window.setTimeout(function(){
			  				ele.contentWindow.changeHtml();
			  			},500)
		  			}(iframs[i]) 	
		  		 }
		  	 }
		},
		//关闭当前窗口
		closeWindow:function(){
			var userAgent = navigator.userAgent;
			if (userAgent.indexOf("Firefox") != -1 || userAgent.indexOf("Presto") != -1) {
			    window.location.replace("about:blank");
			} else {
			    window.opener = null;
			    window.open("", "_self");
			    window.location.href="about:blank";
			    window.close();
			}
		},
		loading:function(type,text){
		  	var type=type||1;
		  	switch(type){
		  		case 1:
		  		var index=layer.msg(text||"数据加载中", {
					icon: 16,
					shade: [0.4, '#ffffff'],
					time:"infinite",
					//offset: [e.elem.offset().top + e.elem.height() / 2 - 135 - S.scrollTop() + "px", e.elem.offset().left + e.elem.width() / 2 - 90 - S.scrollLeft() + "px"],
					anim: -1,
					fixed: !1
				})
			  	return index;
			  	break;
			  	case 2:
			  	var index = layer.load(16, {shade: [0.5,'#fff'] });
			  	return index;
			  	break;
		  	}
		  	
		},
		ajax:function(opt){
		  	if(opt.loading!=false){
				var index = FX.loading(opt.loadingType||1);
		  	} 
		  	$.ajax({
		  		url:opt.url,
			  	type:opt.type||'get',
			  	dataType:opt.dataType||'json',
			  	contentType:'application/json',
			  	data:JSON.stringify(opt.data)||'',
			  	async:opt.async==false?false:true,
			  	headers:{},
			  	success:function(data){
			  		console.log(data);
			  		if(opt.success){
			  			opt.success(data);
			  		}
			  		if(opt.loading!=false){
					  layer.close(index);
		  			}
			  	    
			  	},
			  	error:function(e,t){
			  		layer.close(index);
			  		layer.alert(JSON.stringify(e));
			  		if(opt.error){
			  			opt.error(e,t);
			  		}
			  	}
			  	
		  	})
		  },
		  success:function(msg){
		  	layer.msg(msg);
		  	layui.$('#search-btn').click();
		  },
		  initDate:function(){
		  	layui.$('.bl_date').each(function(k,v){
		  		var _this=this;
				layui.laydate.render({
					elem:this,
					type: $(this).data('type'),
					min: $(this).data('min')||'1900-1-1 00:00:00',
  					max: $(this).data('max')||'2099-12-31',
					done: function(value, date, endDate){
					}
				});
			});
		  },
		  getSearch:function(ary,callback){
		  	var html ='<div class="layui-form-item" style="margin-bottom:0">'
		  	var types={
		  		title:function(title){
		  			html+='<label class="layui-form-label layui-form-labelReset">' + title + '</label>';
		  		},
		  		input:function(obj){
		  			html+='<div class="layui-input-inline">';
		  			html+='<input autocomplete="off" name="'+obj.name+'" type="input" class="layui-input ">';
		  			html+='</div>'
		  		},
		  	}
		  	for(var i =0 ;i<ary.length ;i++){
		  		var item =ary[i].content;
		  		for(var j=0;j<item.length;j++){
		  			html+='<div class="layui-inline">';
		  		    types.title(ary[i].title.name);
		  		    types[item[j].type](item[j]);
		  		    html+='</div>'
		  		}
		  		
		  	}
		    document.getElementById('layui-colla-content').innerHTML = html;
		    layui.form.on('submit(btn-search)', function(data){
			  callback(data.field)
			  return false;
			});
		  },
		  getView:function(obj){
		  	var html='<div class="layui-view">';
		  	for(var i in obj){
		  		html+='<div class="layui-inline"><label class="layui-form-label" style="padding:7px 10px">'+i+'：'+'</label>'
		  		+'<div class="layui-input-inline layui-input-view">'
		  		+ '<input class="layui-input" disabled="disabled" value="' + obj[i] + '">'
		  		+ '</div></div>'
		  	}
		  	html+='</div>';
		  	return html;
		  }
		  
	};


})(window)




